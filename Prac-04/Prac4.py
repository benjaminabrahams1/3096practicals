import busio
import time
import digitalio
import board
import adafruit_mcp3xxx.mcp3008 as MCP
import RPi.GPIO as GPIO # Import Raspberry Pi GPIO library
from adafruit_mcp3xxx.analog_in import AnalogIn
global spi
global cs
global mcp
global chan
global temp
global TStart
global TCurrent
global sleepTime
global buttonCount

def setupboard():
    global spi
    global cs
    global mcp
    global chan
    global temp
    global TStart
    global sleepTime
    global buttonCount
    sleepTime = 10
    buttonCount = 1
    spi = busio.SPI(clock=board.SCK, MISO = board.MISO, MOSI = board.MOSI)
    cs = digitalio.DigitalInOut(board.D5)
    mcp = MCP.MCP3008(spi, cs)
    chan = AnalogIn(mcp, MCP.P0)
    temp = AnalogIn(mcp, MCP.P1)
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(26, GPIO.IN,pull_up_down=GPIO.PUD_UP)
    GPIO.add_event_detect(26, GPIO.FALLING, callback=timeChange, bouncetime=300)
    print("Runtime		Temp reading		Temp			Light Reading")
    TStart = time.time()

def findtemp(tempValue):
    ctemp = temp.voltage
    return ctemp

def timeChange(channel):
    global sleepTime
    global buttonCount
    sleepTime = 10
    if buttonCount <= 3:
        buttonCount = buttonCount + 1
    else:
        buttonCount = 1

    if buttonCount == 1:
        sleepTime = 10
    elif buttonCount == 2:
        sleepTime = 5
    elif buttonCount == 3:
        sleepTime = 1

def printvalues():
    global chan
    tem = (round(temp.voltage,3)-0.5)*100
    t = TCurrent-TStart
    print( str(round(t)) +"s		" + str(round(temp.value,2)) + "			" + str(round(tem,2)) + "C			" + str(round(chan.value)))

if __name__ == "__main__":
    try:
        setupboard()
        while True:
            TCurrent = time.time()
            printvalues()
            time.sleep(sleepTime)
            if GPIO.input(26) == GPIO.LOW:
                print("sup bitch")
    except Exception as e:
        print(e)
    finally:
        GPIO.cleanup()
